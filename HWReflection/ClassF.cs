﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HWReflection
{
    public class ClassF
    {
        public Int32 I1 { get; set; }
        public Int32 I2 { get; set; }
        public Int32 I3 { get; set; }
        public Int32 I4 { get; set; }
        public Int32 I5 { get; set; }

        public ClassF()
        {
            
        }
    }
}
