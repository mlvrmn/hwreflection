﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Diagnostics;

namespace HWReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassF classF = new ClassF() {I1 = 1, I2 = 2, I3 = 3, I4 = 4, I5 = 5};
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();

            for (Int32 i = 0; i < 10000; i++)
            {
                String data = Serializator.ToSerialize(classF);
            }

            stopwatch.Stop();
            Console.WriteLine($"Время на сериализацию в цикле 10000 раз  :{stopwatch.ElapsedMilliseconds} мс.");
            stopwatch.Reset();



            String serializeObject = String.Empty;
            stopwatch.Start();
            for (Int32 i = 0; i < 10000; i++)
            {
                serializeObject = JsonConvert.SerializeObject(classF);
            }
            stopwatch.Stop();
            Console.WriteLine($"Время на сериализацию с помощью библиотеки Newtonsoft" +
                                $" в цикле 10000 раз  :{stopwatch.ElapsedMilliseconds} мс.");

            stopwatch.Reset();

            stopwatch.Start();
            for (Int32 i = 0; i < 10000; i++)
            {
                ClassF classF1 = JsonConvert.DeserializeObject<ClassF>(serializeObject);
            }
            stopwatch.Stop();
            Console.WriteLine($"Время на десериализацию с помощью библиотеки Newtonsoft" +
                                $" в цикле 10000 раз  :{stopwatch.ElapsedMilliseconds} мс.");

            Console.ReadKey();
        }
    }
}
