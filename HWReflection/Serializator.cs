﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;

namespace HWReflection
{
    public static class Serializator
    {
        public static String ToSerialize(Object obj)
        {
            Type type = obj.GetType();
            PropertyInfo[] properties = type.GetProperties();
            StringBuilder builder = new StringBuilder();


            foreach (var property in properties)
            {
                if (properties.Last() == property)
                {
                    builder.Append(property.Name + ", " + property.GetValue(obj));
                    break;
                }
                builder.Append(property.Name + ", " + property.GetValue(obj) + "\n ");
            }

            String data = builder.ToString();

            return data;
        }


        public static Object ToDeserialize<T>(String data)
        {
            Type type = typeof(T);

            PropertyInfo[] properties = type.GetProperties();

            String[] stringArray = data.Split('\n');

            /// тут застял, как сделать механизм превращения строки в любой объект
            return new Object();
        }
    }
}
